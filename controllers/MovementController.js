const io = require ('../io');
const crypt = require ('../crypt');
const accountController = require ('./AccountController');
const datetime = require('../node_modules/node-datetime');
const dateFormat = require('../node_modules/dateformat');


const requestJson = require('request-json');

const mlabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8edrnt/collections/";

const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;


function getMovementsV3 (req, res) {
  console.log("GET /apitechu/v3/products/:id/accounts/:idcuenta/movement");
  console.log(req.params.id);

  var idcliente=req.params.id;
  var idcuenta=req.params.idcuenta;

  var query = 'q={"$and":[{"idcliente": {"$eq":' + idcliente + '}},{"idcuenta": {"$eq":' + idcuenta +'}}]}'
  console.log ("query: " + query);

  var httpClient = requestJson.createClient (mlabBaseURL);
  console.log("Client created");

  console.log (mlabBaseURL + "movement?" + query + "&" + mlabAPIKey);
  httpClient.get("movement?" + query + "&" + mlabAPIKey,
    function (err, resMlab, body) {
      if (err) {
          var response = {
            "msg": "Error obteniendo movimientos"
          };
          res.status(500);
      } else {
        if (body.length > 0) {
          // var response = body[0];
          var response = body;
        } else {
          var response = {
            "msg": "No existen movimientos para esta cuenta"
          };
          res.status(404);
        }
      }
      res.send(response);
      }
)

}


function getSearchMovementsV3 (req, res) {
  console.log("GET /apitechu/v3/products/:id/accounts/:idcuenta/movement/:fecdesde/:fechasta/:impdesde/:imphasta");
  console.log(req.params.id);

  var idcliente=req.params.id;
  var idcuenta=req.params.idcuenta;
  var fechadesde=req.params.fecdesde;
  var fechahasta=req.params.fechasta;
  var importedesde=req.params.impdesde;
  var importehasta=req.params.imphasta;

  var query = 'q={"$and":[{"idcliente": {"$eq":' + idcliente +
              '}},{"idcuenta": {"$eq":' + idcuenta +
              '}},{"fechaoperacion":{"$gte":"' + fechadesde +
              '"}},{"fechaoperacion":{"$lte":"' + fechahasta +
              '"}},{"importe":{"$gte":' + importedesde +
              '}},{"importe":{"$lte":' + importehasta + '}}]}'

  console.log ("query: " + query);

  var httpClient = requestJson.createClient (mlabBaseURL);
  console.log("Client created");

  console.log (mlabBaseURL + "movement?" + query + "&" + mlabAPIKey);
  httpClient.get("movement?" + query + "&" + mlabAPIKey,
    function (err, resMlab, body) {
      if (err) {
          var response = {
            "msg": "Error obteniendo movimientos"
          };
          res.status(500);
      } else {
        if (body.length > 0) {
          // var response = body[0];
          var response = body;
        } else {
          var response = {
            "msg": "No existen movimientos que cumplan esos criterios"
          };
          res.status(404);
        }
      }
      res.send(response);
      }
    )
}

function getDetailMovementsV3 (req, res) {
  console.log("GET /apitechu/v3/products/:id/accounts/:idcuenta/movement/:idmov");
  console.log(req.params.id);

  var idcliente=req.params.id;
  var idcuenta=req.params.idcuenta;
  var idmovimiento=req.params.idmov;

  var query = 'q={"$and":[{"idcliente": {"$eq":' + idcliente + '}},{"idcuenta": {"$eq":' + idcuenta +'}},{"tmsmovimiento": {"$eq":' + idmovimiento +'}}]}'
  console.log ("query: " + query);

  var httpClient = requestJson.createClient (mlabBaseURL);
  console.log("Client created");

  console.log (mlabBaseURL + "movement?" + query + "&" + mlabAPIKey);
  httpClient.get("movement?" + query + "&" + mlabAPIKey,
    function (err, resMlab, body) {
      if (err) {
          var response = {
            "msg": "Error obteniendo el movimiento"
          };
          res.status(500);
      } else {
        if (body.length > 0) {
          // var response = body[0];
          var response = body;
        } else {
          var response = {
            "msg": "No existe ese movimiento para esta cuenta"
          };
          res.status(404);
        }
      }
      res.send(response);
      }
)

}
function ingresoV3 (req, res) {
  console.log("POST /apitechu/v3/ingreso");

  createMovementV3(req,res);
  // accountController.actualizarsaldoV3(req, res);

}


function createMovementV3 (req, res) {
  console.log("POST /apitechu/v3/products/:id/accounts/:idcuenta/movement");

  console.log(req.body.fechaoperacion);
  // var fechaoperacion = dateFormat(new Date(), "dd/mm/yyyy");
  var fechaoperacion = dateFormat(new Date(), "yyyy-mm-dd");
  console.log("fechaoperacion: ", fechaoperacion);
  var dt = datetime.create();
  var tmsmovimiento = dt.now();
  console.log("tmsmovimiento: ", tmsmovimiento);

  var fechavalor = fechaoperacion;

  console.log(req.body.idcliente);
  console.log(req.body.idcuenta);
  console.log(req.body.importe);
  console.log(req.body.divisa);
  console.log(req.body.texto);
  console.log(req.body.latitude);
  console.log(req.body.longitude);
  // var importe = req.body.importe.toFixed(2);
  // console.log("importe:", importe);

  var newMovement = {
      "fechaoperacion" : fechaoperacion,
      "tmsmovimiento"  : tmsmovimiento,
      "idcliente"      : req.body.idcliente,
      "idcuenta"       : req.body.idcuenta,
      // "importe"        : parseFloat(req.body.importe),
      "importe"        : req.body.importe,
      "divisa"         : req.body.divisa,
      "texto"          : req.body.texto,
      "fechavalor"     : fechavalor,
      "latitude"       : req.body.latitude,
      "longitude"      : req.body.longitude

  };

  var httpClient = requestJson.createClient (mlabBaseURL);
  console.log("Client created");

  httpClient.post("movement?" + mlabAPIKey, newMovement,
    function (err, resMlab, body) {
      console.log("Movimiento guardado con éxito");

//actualizamos saldo en la tabla de cuentas
      accountController.actualizarsaldoV3(req, res);

      res.status(201);
      res.send({"msg": "Movimiento creado con éxito"});
    }

  )

}

module.exports.getMovementsV3 = getMovementsV3;
module.exports.getSearchMovementsV3 = getSearchMovementsV3;
module.exports.getDetailMovementsV3 = getDetailMovementsV3;
module.exports.createMovementV3 = createMovementV3;
module.exports.ingresoV3 = ingresoV3;
