const io = require ('../io');
const crypt = require ('../crypt');
const datetime = require('../node_modules/node-datetime');
const dateFormat = require('../node_modules/dateformat');

const requestJson = require('request-json');

const mlabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8edrnt/collections/";

const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;




function getDetailUserV3 (req, res) {
  console.log("GET /apitechu/v3/users/:id");

  var dt = datetime.create();
  console.log("dt: ", dt.now());

  var fechaalta=dateFormat(new Date(), "dd/mm/yyyy");
  console.log("day: ", fechaalta);


  var idcliente=req.params.id;
//  console.log ("id: " + id);
  var query = 'q={"idcliente":' + idcliente + '}';
//    console.log ("query: " + query);

  var httpClient = requestJson.createClient (mlabBaseURL);
  console.log("Client created");

//    console.log (mlabBaseURL + "user?" + query + "&" + mlabAPIKey);
  httpClient.get("user?" + query + "&" + mlabAPIKey,
    function (err, resMlab, body) {
      if (err) {
          var response = {
            "msg": "Error obteniendo usuario"
          };
          res.status(500);
      } else {
        if (body.length > 0) {
          var response = body[0];
          res.status(200);

        } else {
          var response = {
            "msg": "Usuario no encontrado"
          };
          res.status(404);
        }

      }

      res.send(response);

//        var response = !err ?
//          body :  {"msg": "Error obteniendo usuarios"}
//
//        res.send(response);
    }

)

}


function createUserV3 (req, res) {
  console.log("POST /apitechu/v3/users");

  var dni = req.body.dni;
  var query = 'q={"dni":' + '"' + dni + '"}';

  var httpClient = requestJson.createClient(mlabBaseURL);
  console.log("Client created");

  console.log(mlabBaseURL + "acceso?" + query + "&" + mlabAPIKey);

  var dniNoRegistrado = false;

  httpClient.get("acceso?" + query + "&" + mlabAPIKey,
    function(err, resMlab, body) {
      if (err) {
        var response = {
          "msg": "Error obteniendo usuario"
        };
        res.status(500);
        res.send(response);
      } else {
        if (body.length == 0) {
          console.log("OK.DNI no existe");
          dniNoRegistrado = true;
        } else {
          var response = {
            "msg": "El DNI ya está registrado!"
          };
          res.status(400);
          res.send(response);
        }
      }

      if (dniNoRegistrado) {
      // Obtener siguiente idcliente disponible.
        var query = 's={"idcliente":-1}';
        var nexidcliente=0;

        var httpClient = requestJson.createClient(mlabBaseURL);
        console.log("Client created");

        console.log(mlabBaseURL + "acceso?" + query + "&" + mlabAPIKey);

        httpClient.get("acceso?" + query + "&" + mlabAPIKey,
          function (err, resMlab, body) {
            if (err) {
                var response = {
                  "msg": "Error obteniendo id para el nuevo usuario"
                };
                res.status(500);
                res.send(response);
            } else {
              if (body.length > 0) {
                console.log("Último id:" ,body[0].idcliente);
                nexidcliente = body[0].idcliente + 1;
              } else {
                nexidcliente = body[0].idcliente + 1;
              }

            }

            console.log("Next idcliente:" , nexidcliente);
            console.log(req.body.dni);
            console.log(req.body.email);
            console.log(req.body.password);
            console.log(req.body.nombre);
            console.log(req.body.primer_apellido);
            console.log(req.body.segundo_apellido);
            console.log(req.body.genero);
            console.log(req.body.telefono);
            console.log(req.body.direccion);
            console.log(req.body.poblacion);
            console.log(req.body.provincia);
            console.log(req.body.codigopostal);
            console.log(req.body.foto);
            var fechaalta=dateFormat(new Date(), "dd/mm/yyyy");
            console.log("fechaalta: ", fechaalta);
            // console.log(req.body.fechaalta);

            var newUser = {
                "idcliente"        : nexidcliente,
                "nombre"           : req.body.nombre,
                "primer_apellido"  : req.body.primer_apellido,
                "segundo_apellido" : req.body.segundo_apellido,
                "email"            : req.body.email,
                "telefono"         : req.body.telefono,
                "genero"           : req.body.genero,
                "direccion"        : req.body.direccion,
                "poblacion"        : req.body.poblacion,
                "provincia"        : req.body.provincia,
                "codigopostal"     : req.body.codigopostal,
                "foto"             : req.body.foto,
                "fechaalta"        : fechaalta
            };

            httpClient.post("user?" + mlabAPIKey, newUser,
              function (err, resMlab, body) {
                console.log("Usuario guardado con éxito en tabla users");

                console.log("dni: " ,req.body.dni);
                console.log("password: " ,req.body.password);

                var newAcceso = {
                    "idcliente"  : nexidcliente,
                    "dni"        : req.body.dni,
                    "password"   : crypt.hash(req.body.password),
                    "situacion"  : 0,
                    "fechabaja"  :"31/12/9999",
                    "logged"     : true
                };

                httpClient.post("acceso?" + mlabAPIKey, newAcceso,
                  function (errPOST, resMlabPOST, bodyPOST) {
                    console.log("Usuario creado y loggeado con éxito");

                    // var query = 'q={"idcliente":' + idcliente + '}';
                    // var putBody = '{"$set":{"logged":true}}';

                    // lanzo la petición
                    // httpClient.put("acceso?" + query + "&" + mlabAPIKey, JSON.parse(putBody),
                    //   function(errPUT, resMlabPUT, bodyPUT) {
                        console.log("Usuario creado y loggeado con éxito");
                        var response = {
                          "msg": "Usuario creado y loggeado con éxito",
                          "idUsuario": nexidcliente
                        }
                        res.status(200);
                        res.send(response);
                      // });
                  }
                )
              }
            )
          }
        )
      }
    }
  )
}

function updateUserV3 (req, res) {
    console.log("POST /apitechu/v3/users/:id");

 //  console.log(req.headers);
  console.log(req.params.id);
  console.log(req.body.nombre);
  console.log(req.body.primer_apellido);
  console.log(req.body.segundo_apellido);
  console.log(req.body.email);
  console.log(req.body.telefono);
  console.log(req.body.genero);
  console.log(req.body.direccion);
  console.log(req.body.poblacion);
  console.log(req.body.provincia);
  console.log(req.body.codigopostal);
  console.log(req.body.foto);
  console.log(req.body.fechaalta);


  var updateUser = {
    // parseo el parametro de id de cliente para que sea numérico
      "idcliente"        : parseInt(req.params.id,10),
      "nombre"           : req.body.nombre,
      "primer_apellido"  : req.body.primer_apellido,
      "segundo_apellido" : req.body.segundo_apellido,
      "email"            : req.body.email,
      "telefono"         : req.body.telefono,
      "genero"           : req.body.genero,
      "direccion"        : req.body.direccion,
      "poblacion"        : req.body.poblacion,
      "provincia"        : req.body.provincia,
      "codigopostal"     : req.body.codigopostal,
      "foto"             : req.body.foto,
      "fechaalta"        : req.body.fechaalta
  };

  var idcliente=req.params.id;
  var query = 'q={"idcliente":' + idcliente + '}';

  var httpClient = requestJson.createClient (mlabBaseURL);
  console.log("Client created");

  httpClient.put("user?" + query + "&" + mlabAPIKey, updateUser,
    function (err, resMlab, body) {
      console.log("Usuario guardado con éxito");
      res.status(201);
      res.send({"msg": "Usuario guardado con éxito"});
    }

  )
}



function deleteUserV3 (req, res) {
  console.log("DELETE /apitechu/v3/users/:id");

  var dt = datetime.create();
  console.log("dt: ", dt.now());

  var idcliente=req.params.id;
//  console.log ("id: " + id);
  var query = 'q={"idcliente":' + idcliente + '}';
//    console.log ("query: " + query);

  var httpClient = requestJson.createClient (mlabBaseURL);
  console.log("Client created");

 console.log (mlabBaseURL + "user?" + query + "&" + mlabAPIKey);
  httpClient.delete("user?" + query + "&" + mlabAPIKey,
    function (err, resMlab, body) {
      console.log("err:", err);
      // console.log("resMlab:", resMlab);
      console.log("body:", body);
      if (err) {
          var response = {
            "msg": "Error obteniendo usuario"
          };
          res.status(500);
      } else {
          console.log("usuario borrado con exito");
          var response = {
            "msg": "Usuario borrado con éxito"
          };
          res.status(200);
        }

      res.send(response);

    }

)

}

  module.exports.getDetailUserV3 = getDetailUserV3;
  module.exports.createUserV3 = createUserV3;
  module.exports.updateUserV3 = updateUserV3;
  module.exports.deleteUserV3 = deleteUserV3;
