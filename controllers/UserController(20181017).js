const io = require ('../io');
const crypt = require ('../crypt');

const requestJson = require('request-json');

const mlabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8edrnt/collections/";

const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function getUsersV1 (req, res) {
  console.log("GET /apitechu/v1/users");

  var result = {};
  var users = require('../usuarios.json');

  if (req.query.$count == "true") {
    console.log("Count needed");
    result.count = users.length;
  }

  result.users = req.query.$top ?
    users.slice(0, req.query.$top) : users;

    res.send(result);
  }

  function getUsersV2 (req, res) {
    console.log("GET /apitechu/v2/users");

    var httpClient = requestJson.createClient (mlabBaseURL);
    console.log("Client created");

    httpClient.get("user?" + mlabAPIKey,
      function (err, resMlab, body) {
        var response = !err ?
          body :  {"msg": "Error obteniendo usuarios"}

        res.send(response);
      }

  )

  }

  function getUserByIdV2 (req, res) {
    console.log("GET /apitechu/v2/users/:id");

    var id=req.params.id;
  //  console.log ("id: " + id);
    var query = 'q={"id":' + id + '}';
//    console.log ("query: " + query);

    var httpClient = requestJson.createClient (mlabBaseURL);
    console.log("Client created");

//    console.log (mlabBaseURL + "user?" + query + "&" + mlabAPIKey);
    httpClient.get("user?" + query + "&" + mlabAPIKey,
      function (err, resMlab, body) {
        if (err) {
            var response = {
              "msg": "Error obteniendo usuario"
            };
            res.status(500);
        } else {
          if (body.length > 0) {
            var response = body[0];
          } else {
            var response = {
              "msg": "Usuario no encontrado"
            };
            res.status(404);
          }

        }

        res.send(response);

//        var response = !err ?
//          body :  {"msg": "Error obteniendo usuarios"}
//
//        res.send(response);
      }

  )

}

  function createUserV2 (req, res) {
    console.log("POST /apitechu/v2/users");

 //  console.log(req.headers);
  console.log(req.body.id);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);
  console.log(req.body.password);


  var newUser = {
      "id"         : req.body.id,
      "first_name" : req.body.first_name,
      "last_name"  : req.body.last_name,
      "email"      : req.body.email,
      "password"   : crypt.hash(req.body.password)
  };

  var httpClient = requestJson.createClient (mlabBaseURL);
  console.log("Client created");

  httpClient.post("user?" + mlabAPIKey, newUser,
    function (err, resMlab, body) {
      console.log("Usuario guardado con éxito");
      res.status(201);
      res.send({"msg": "Usuario creado con éxito"});
    }

  )
}

function createUserV1 (req, res) {
  console.log("POST /apitechu/v1/users");

//  console.log(req.headers);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);
  console.log(req.body.password);


  var newUser = {
      "first_name" : req.body.first_name,
      "last_name"  : req.body.last_name,
      "email"      : req.body.email,
      "password"   : req.body.password
  };

  var users = require("../usuarios.json");

  users.push(newUser);

  io.writeUserDataToFile(users);

  res.send ("Usuario añadido con éxito");


}



    function deleteUserV1 (req, res) {
       console.log("DELETE /apitechu/v1/users/:id");

       console.log("La id enviada es: " +  req.params.id);

       var users = require("../usuarios.json");
       var deleted = false;

       console.log("users.length: " +  users.length);


 // bucle for

       for (var i=0; i < users.length; i++)
       {
 //          console.log ("users[i]= " + users[i]);
         console.log ("users[i].id= " + users[i].id);
         console.log ("req.params.id= " + req.params.id);

         if (users[i].id == req.params.id)
         {
            users.splice(i, 1);
            console.log ("Usuario borrado");
            deleted = true;
            break;
         }
       }
       if (deleted) {
          io.writeUserDataToFile(users);
       }

       var msg = deleted ? "Usuario borrado" : "Usuario no encontrado."
       res.send ({"msg" : msg});

     }

  module.exports.getUsersV1 = getUsersV1;
  module.exports.getUsersV2 = getUsersV2;
  module.exports.getUserByIdV2 = getUserByIdV2;

  module.exports.createUserV1 = createUserV1;
  module.exports.createUserV2 = createUserV2;
  module.exports.deleteUserV1 = deleteUserV1;
