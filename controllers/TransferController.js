const io = require ('../io');
const datetime = require('../node_modules/node-datetime');
const dateFormat = require('../node_modules/dateformat');


const requestJson = require('request-json');

const mlabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8edrnt/collections/";

const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;


function getTransfersV3 (req, res) {
  console.log("GET /apitechu/v3/products/:id/accounts/:idcuenta/transfer");

  var idcliente=req.params.id;
  var idcuenta=req.params.idcuenta;
  console.log(idcliente);
  console.log(idcuenta);

  var query = 'q={"$and":[{"idcliente": {"$eq":' + idcliente + '}},{"idcuenta": {"$eq":' + idcuenta +'}}]}'
  console.log ("query: " + query);

  var httpClient = requestJson.createClient (mlabBaseURL);
  console.log("Client created");

  console.log (mlabBaseURL + "tranfer?" + query + "&" + mlabAPIKey);
  httpClient.get("transfer?" + query + "&" + mlabAPIKey,
    function (err, resMlab, body) {
      if (err) {
          var response = {
            "msg": "Error obteniendo transferencias"
          };
          res.status(500);
      } else {
        if (body.length > 0) {
          // var response = body[0];
          var response = body;
        } else {
          var response = {
            "msg": "No existen transferencias para esta cuenta"
          };
          res.status(404);
        }
      }
      res.send(response);
      }
)

}


function createTransferV3 (req, res) {
  console.log("POST /apitechu/v3/products/:id/accounts/:idcuenta/transfer");

  var fechaoperacion = dateFormat(new Date(), "dd/mm/yyyy");
  console.log("fechaoperacion: ", fechaoperacion);
  var dt = datetime.create();
  var tmstransferencia = dt.now();
  console.log("tmstransferencia: ", tmstransferencia);

  var fechavalor = fechaoperacion;

  console.log(req.params.id);
  console.log(req.params.idcuenta);
  console.log(req.body.importe);
  console.log(req.body.divisa);
  console.log(req.body.concepto);
  console.log(req.body.ibandestino);

  // console.log(parseFloat(req.body.importe));
  var importe = parseFloat((Math.round(req.body.importe * 100) / 100).toFixed(2));
  console.log("importe:", importe);

  var newTransfer = {
      "fechaoperacion" : fechaoperacion,
      "tmstransferencia"  : tmstransferencia,
      "idcliente"      : parseInt(req.params.id),
      "idcuenta"       : parseInt(req.params.idcuenta),
      "importe"        : importe,
      "divisa"         : req.body.divisa,
      "concepto"       : req.body.concepto,
      "fechavalor"     : fechavalor,
      "ibandestino"    : req.body.ibandestino

  };

  var httpClient = requestJson.createClient (mlabBaseURL);
  console.log("Client created");

  httpClient.post("transfer?" + mlabAPIKey, newTransfer,
    function (err, resMlab, body) {
      console.log("Transferencia guardada con éxito");

      res.status(201);
      res.send({"msg": "Transferencia guardada con éxito"});
    }
  )
}

module.exports.getTransfersV3 = getTransfersV3;
module.exports.createTransferV3 = createTransferV3;
