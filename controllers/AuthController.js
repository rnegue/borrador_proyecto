const io = require('../io');
const crypt = require('../crypt');

const requestJson = require('request-json');

const mlabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8edrnt/collections/";

const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function loginV3(req, res) {
/*
  Autor: Roberto Negueruela - 3/11/2018
  Descripción: Realizada el Login en la aplicación a partir de un DNI y
               una Password.
  Validaciones: - que el usuario no esté previamente loggeado.
                - que el usuario esté registrado
                - que la password sea correcta.
*/
  console.log("POST /apitechu/v3/login");
  console.log("dni: ", req.body.dni);
//
  var dni = req.body.dni;
  var query = 'q={"dni":' + '"' + dni + '"}';

  console.log("query: ", query);

  var httpClient = requestJson.createClient(mlabBaseURL);
  console.log("Client created");

  console.log(mlabBaseURL + "acceso?" + query + "&" + mlabAPIKey);

  var usuarioEncontrado = false;
  var clavecorrecta = false;

  httpClient.get("acceso?" + query + "&" + mlabAPIKey,
    function(err, resMlab, body) {
      if (err) {
        var response = {
          "msg": "Error obteniendo usuario"
        };
        res.status(500);
        res.send(response);
      } else {
          if (body.length > 0) {
            if (body[0].logged) {                   // si el usuario ya está conectado
              var response = {
                "msg": "Usuario ya está loggeado!"
              };
              res.status(400);
              res.send(response);
            } else {
                console.log("usuario encontrado");
                var user = body[0];
                usuarioEncontrado = true;
              }
          } else {
              var response = {
                "msg": "Up!, me temo que este usuario no está registrado en la aplicación!"
              };
              res.status(404);
              res.send(response);
            }
        }

      if (usuarioEncontrado) {
        var passwordOK = crypt.checkpassword(req.body.password, user.password)

        if (passwordOK) {
          // Parametro para poner variable logged a true
          var putBody = '{"$set":{"logged":true}}';
          httpClient.put("acceso?" + query + "&" + mlabAPIKey, JSON.parse(putBody),
            function(errPUT, resMlabPUT, bodyPUT) {
              console.log("Usuario loggeado con éxito");
              var response = {
                "msg": "Usuario loggeado con éxito",
                "idUsuario": body[0].idcliente
              }
              res.send(response);
            });
        }
        else{
          console.log("Clave incorrecta");
          var response = {
            "msg": "Clave incorrecta"
          }
          res.status(401);
          res.send(response);
        }
      }
    }
  );
}


function bajaUsuarioV3 (req, res) {
/*
  Autor: Roberto Negueruela - 3/11/2018
  Descripción: Da de baja al usuario activando la marca de baja.
               No es baja física.
  Validaciones: - que el usuario no tenga cuentas activas
                - que el usuario esté loggeado
                - que el usuario no esté a inactivo
*/

  console.log("POST /apitechu/v3/bajausuario");

  var idcliente = req.params.id;
  var query = 'q={"idcliente":' + idcliente + '}&c=true';

  console.log("query: ", query);

  var httpClient = requestJson.createClient(mlabBaseURL);
  console.log("Client created");

  console.log(mlabBaseURL + "account?" + query + "&" + mlabAPIKey);

  var usuarioEncontrado = false;
  var clavecorrecta = false;
  var contadorCuentas = 0;

  httpClient.get("account?" + query + "&" + mlabAPIKey,
    function(err, resMlab, body) {
      if (err) {
        var response = {
          "msg": "Error obteniendo usuario"
        };
        res.status(500);
        res.send(response);
      }

      contadorCuentas = body;
      console.log("contadorCuentas :", contadorCuentas);

      if (contadorCuentas!=0) {
        var response = {
          "msg": "Usuario tiene cuentas. Para darse de baja primero debe cancelar sus cuentas"
        };
        res.status(401);
        res.send(response);
      } else {
          var query = 'q={"idcliente":' + idcliente + '}';
          var usuarioEncontrado = false;
          var clavecorrecta = false;

          console.log(mlabBaseURL + "acceso?" + query + "&" + mlabAPIKey);

          httpClient.get("acceso?" + query + "&" + mlabAPIKey,
            function(err, resMlab, body) {
              if (err) {
                var response = {
                  "msg": "Error obteniendo usuario"
                };
                res.status(500);
                res.send(response);
              } else {
                if (body.length > 0) {
                  console.log("usuario encontrado");
                  var user = body[0];
                  usuarioEncontrado = true;
                } else {
                      var response = {
                        "msg": "Usuario no encontrado"
                      };
                      res.status(404);
                      res.send(response);
                  }
                }

              if (usuarioEncontrado) {
                if (!user.logged) {
                  var response = {
                    "msg": "Usuario no loggeado. No puede darse de baja"
                  };
                  res.status(401);
                  res.send(response);
                }
                else {
                  if (user.baja) {
                    var response = {
                      "msg": "Usuario ya esta inactivo. No aplica volver a darse de baja"
                    };
                    res.status(401);
                    res.send(response);
                  }
                  else {
                    // Parametro para poner variable logged a true
                    var putBody = '{"$set":{"baja":true}}';
                    // lanzo la petición
                    console.log(mlabBaseURL + "acceso?" + query + "&" + mlabAPIKey);

                    httpClient.put("acceso?" + query + "&" + mlabAPIKey, JSON.parse(putBody),
                      function(errPUT, resMlabPUT, bodyPUT) {
                        console.log("Usuario dado de baja con éxito");
                        var response = {
                          "msg": "Usuario dado de baja con éxito",
                          "idUsuario": idcliente
                        }
                        res.send(response);
                      }
                    );
                  }
                }
              }
            }
          )
        }
    }
  );
}

function reactivarUsuarioV3 (req, res) {
/*
  Autor: Roberto Negueruela - 3/11/2018
  Descripción: Reactiva un usuario que esté dado de baja

  Validaciones: - que el usuario exista
                - que el usuario esté loggeado
                - que el usuario esté en situación de baja
*/

  console.log("POST /apitechu/v3/reactivar/:id");

  var idcliente = req.params.id;

  var httpClient = requestJson.createClient(mlabBaseURL);
  console.log("Client created");

  var query = 'q={"idcliente":' + idcliente + '}';                //buscamos cliente en tabla acceso por su id
  var usuarioEncontrado = false;
  var clavecorrecta = false;

  console.log(mlabBaseURL + "acceso?" + query + "&" + mlabAPIKey);

  httpClient.get("acceso?" + query + "&" + mlabAPIKey,
    function(err, resMlab, body) {
      if (err) {
        var response = {
          "msg": "Error obteniendo usuario"
        };
        res.status(500);
        res.send(response);
      } else {
          if (body.length > 0) {                        //cliente encontrado
            console.log("usuario encontrado");
            var user = body[0];
            usuarioEncontrado = true;
          } else {
              var response = {
                "msg": "Usuario no encontrado"
              };
              res.status(404);
              res.send(response);
            }
        }

      if (usuarioEncontrado) {
        if (!user.logged) {
          var response = {
            "msg": "Usuario no loggeado. No puede ser reactivado"
          };
          res.status(401);
          res.send(response);
        }
        else {
          if (!user.baja) {
            var response = {
              "msg": "Usuario no está inactivo. No aplica reactivarlo"
            };
            res.status(401);
            res.send(response);
          }
          else {
            var putBody = '{"$unset":{"baja":true}}';           //desmarcamos la baja volver a situación activa
            console.log(mlabBaseURL + "acceso?" + query + "&" + mlabAPIKey);
            httpClient.put("acceso?" + query + "&" + mlabAPIKey, JSON.parse(putBody),
              function(errPUT, resMlabPUT, bodyPUT) {
                console.log("Usuario reactivado con éxito");
                var response = {
                  "msg": "Usuario reactivado con éxito",
                  "idUsuario": idcliente
                }
                res.send(response);
            });
          }
        }
      }
    });
}


function logoutV3(req, res) {
  console.log("POST /apitechu/v3/logout/:id");

// recibimos el id para logout como parametro en url en lugar de en el body
  var id=req.params.id;
  // var id = req.body.id;
  console.log("idcliente: " + id);
  var query = 'q={"idcliente":' + id + '}';

  var httpClient = requestJson.createClient(mlabBaseURL);
  console.log("Client created");

  console.log(mlabBaseURL + "acceso?" + query + "&" + mlabAPIKey);

  var usuarioLogged = false;

  httpClient.get("acceso?" + query + "&" + mlabAPIKey,
    function(err, resMlab, body) {
      if (err) {
        var response = {
          "msg": "Error obteniendo usuario"
        };
        res.status(500);
      } else {
        if (body.length > 0) {
          console.log("usuario encontrado");
          var user = body[0];
          console.log("user.logged: ", user.logged);
          if (user.logged) {
            usuarioLogged = true;
          } else {
            var response = {
              "msg": "Usuario no loggeado",
              "idcliente": user.idcliente
            }
            console.log("Usuario no loggeado");
            // 401 - no permitido
            res.status(401);
            res.send(response);
          }
        } else {
          console.log ("¡Qué extraño!,no existe el usuario que solicita deslogarse!");
          var response = {
            "msg": "¡Qué extraño!,no existe el usuario que solicita deslogarse!",
            "idcliente": req.params.id
          };
          res.status(404);
          res.send(response);
        }
      }
      console.log("usuarioLogged:", usuarioLogged);
      if (usuarioLogged) {

        // Parametro para poner variable logged a true
        var putBody = '{"$unset":{"logged":""}}';
        // lanzo la petición
        httpClient.put("acceso?" + query + "&" + mlabAPIKey, JSON.parse(putBody),
          function(errPUT, resMlabPUT, bodyPUT) {
            var response = {
              "msg": "Usuario desloggeado con éxito",
              "idcliente": user.idcliente
            }
            console.log("Usuario logout con éxito");
            res.send(response);

          });
      }
    });
}

function changepasswordV3 (req, res) {
  console.log("POST /apitechu/v3/changepassword/:id");

// recibimos el id para logout como parametro en url en lugar de en el body
  var id=req.params.id;
  console.log("old password: " ,req.body.oldpassword);
  console.log("new password: " ,req.body.newpassword);  // var id = req.body.id;
  console.log("idcliente: " + id);

  var query = 'q={"idcliente":' + id + '}';

  var httpClient = requestJson.createClient(mlabBaseURL);
  console.log("Client created");

  console.log(mlabBaseURL + "acceso?" + query + "&" + mlabAPIKey);

  var usuarioLogged = false;
  var clavecorrecta = false;

  httpClient.get("acceso?" + query + "&" + mlabAPIKey,
    function(err, resMlab, body) {
      if (err) {
        var response = {
          "msg": "Error obteniendo usuario"
        };
        res.status(500);
        res.send(response);
      } else {
          if (body.length > 0) {
            var user = body[0];
            if (user.logged) {
              usuarioLogged = true;
            }
            else{
              console.log("Usuario no loggeado");
              var response = {
                "msg": "Usuario no loggeado"
              }
              res.status(401);
              res.send(response);
            }
          } else {
              console.log("entra en else");
              var response = {
                "msg": "Usuario no encontrado o no loggeado"
              };
              res.status(404);
              res.send(response);
            }
        }

      if (usuarioLogged) {
        var passwordOK = crypt.checkpassword(req.body.oldpassword, user.password)

        if (passwordOK) {
          // Parametro para poner variable logged a true
          var updatePassword = {
              "idcliente"  : user.idcliente,
              "dni"        : user.dni,
              "password"   : crypt.hash(req.body.newpassword),
              "situacion"  : 0,
              "fechabaja"  :"31/12/9999",
              "logged"     :true
          };

          httpClient.put("acceso?" + query + "&" + mlabAPIKey, updatePassword,
            function (err, resMlab, body) {
              console.log("Password actualizada con éxito");
              res.status(201);
              res.send({"msg": "Password actualizada con éxito"});
            }
          )
        }
        else{
          console.log("Clave antigua errónea");
          var response = {
            "msg": "Clave antigua errónea"
          }
          res.status(401);
          res.send(response);
        }
      }
    }
  );
}

function getDetailAccesoV3 (req, res) {
  console.log("GET /apitechu/v3/acceso/:id");

  var idcliente=req.params.id;
//  console.log ("id: " + id);
  var query = 'q={"idcliente":' + idcliente + '}';
//    console.log ("query: " + query);

  var httpClient = requestJson.createClient (mlabBaseURL);
  console.log("Client created");

//    console.log (mlabBaseURL + "user?" + query + "&" + mlabAPIKey);
  httpClient.get("acceso?" + query + "&" + mlabAPIKey,
    function (err, resMlab, body) {
      if (err) {
          var response = {
            "msg": "Error obteniendo usuario"
          };
          res.status(500);
      } else {
          if (body.length > 0) {
            var response = body[0];

          } else {
              var response = {
                "msg": "Usuario no encontrado"
              };
              res.status(404);
            }

        }

      res.send(response);
    }
  )
}

function logoutV1(req, res) {
  console.log("POST /apitechu/v1/logout");

  var users = require('../usuarios.json');
  for (user of users) {
    if (user.id == req.body.id && user.logged === true) {
      console.log("User found, logging out");
      delete user.logged
      console.log("Logged out user with id " + user.id);
      var loggedoutUserId = user.id;
      io.writeUserDataToFile(users);
      break;
    }
  }

  var msg = loggedoutUserId ?
    "Logout correcto" : "Logout incorrecto";

  var response = {
    "mensaje": msg,
    "idUsuario": loggedoutUserId
  };

  res.send(response);
}

// module.exports.loginV1 = loginV1;
// module.exports.loginV2 = loginV2;
module.exports.loginV3 = loginV3;
module.exports.logoutV1 = logoutV1;
module.exports.logoutV3 = logoutV3;
module.exports.changepasswordV3 = changepasswordV3;
module.exports.getDetailAccesoV3 = getDetailAccesoV3;
module.exports.bajaUsuarioV3 = bajaUsuarioV3;
module.exports.reactivarUsuarioV3 = reactivarUsuarioV3;
