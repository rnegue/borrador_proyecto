const io = require ('../io');
const crypt = require ('../crypt');
const datetime = require('../node_modules/node-datetime');
const dateFormat = require('../node_modules/dateformat');

const requestJson = require('request-json');

const exchangeBaseURL = "http://api.cambio.today/v1/quotes/";

const exchangeAPIKey = "apiKey=" + process.env.EXCHANGE_API_KEY;


function getexchangesV3 (req, res) {
// Recupera contravalor entre divisas

  console.log("GET/apitechu/v3/exchanges");

  var sourceCurrency=req.params.sourceCurrency;
  var tarjetCurrency=req.params.tarjetCurrency;
  var quantity= req.params.quantity;
  console.log("req.params.quantity", req.params.quantity);
  console.log("quantity", quantity);
  //  console.log ("id: " + id);
  var query = sourceCurrency + '/' + tarjetCurrency + '/json?quantity=' + quantity ;
//    console.log ("query: " + query);

  var httpClient = requestJson.createClient (exchangeBaseURL);
  console.log("Client created");

  console.log (exchangeBaseURL + query + "&key=" + exchangeAPIKey);
  httpClient.get(query + "&key=" + exchangeAPIKey,
    function (err, resCurrency, body) {
      if (err) {
          var response = {
            "msg": "Error obteniendo cambio divisas"
          };
          res.status(500);
      } else {
        console.log("body", body);
        if (body.status == 'OK') {
          var response = body.result;
        } else {
          var response = {
            "msg": "No existen cambio para esa divisa"
          };
          res.status(404);
        }
      }
      res.send(response);
    }
  )
  }

  module.exports.getexchangesV3 = getexchangesV3;
