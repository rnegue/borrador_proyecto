const io = require ('../io');
const crypt = require ('../crypt');
const datetime = require('../node_modules/node-datetime');
const dateFormat = require('../node_modules/dateformat');

const requestJson = require('request-json');

const mlabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8edrnt/collections/";

const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;


function getAccountsV3 (req, res) {
// Recupera lista de cuenta de un cliente

  console.log("GET/apitechu/v3/products/:id/accounts");

  var idcliente=req.params.id;
  //  console.log ("id: " + id);
  var query = 'q={"idcliente":' + idcliente + '}';
//    console.log ("query: " + query);

  var httpClient = requestJson.createClient (mlabBaseURL);
  console.log("Client created");

  console.log (mlabBaseURL + "account?" + query + "&" + mlabAPIKey);
  httpClient.get("account?" + query + "&" + mlabAPIKey,
    function (err, resMlab, body) {
      if (err) {
          var response = {
            "msg": "Error obteniendo cuentas del cliente"
          };
          res.status(500);
      } else {
        if (body.length > 0) {
          // var response = body[0];
          var response = body;
        } else {
          var response = {
            "msg": "No existen cuentas para el cliente"
          };
          res.status(404);
        }
      }
      res.send(response);
    }

  )
  }
function getAccountsbyibanV3 (req, res) {
// Recupera lista de cuenta de un cliente

  console.log("GET/apitechu/v3/accounts/:iban");

  var iban=req.params.iban;
  //  console.log ("id: " + id);
  var query = 'q={"iban":"' + iban + '"}';
//    console.log ("query: " + query);

  var httpClient = requestJson.createClient (mlabBaseURL);
  console.log("Client created");

  console.log (mlabBaseURL + "account?" + query + "&" + mlabAPIKey);
  httpClient.get("account?" + query + "&" + mlabAPIKey,
    function (err, resMlab, body) {
      if (err) {
          var response = {
            "msg": "Error obteniendo cuenta destino"
          };
          res.status(500);
      } else {
        if (body.length > 0) {
          // var response = body[0];
          var response = body[0];
        } else {
          var response = {
            "msg": "No existe esa cuenta destino de la transferencia"
          };
          res.status(404);
        }
      }
      res.send(response);
    }

  )
  }

  function getDetailAccountV3 (req, res) {
  // Recupera detalle de una cuenta concreta de un cliente concreto
    console.log("/apitechu/v3/products/:id/accounts/:idcuenta");

    //OJO!!: HE MODIFICADO PARA USARLA A AL HORA DE ACTUALIZAR SALDO Y
    //POR ESO HE PUESTO BODY PERO ... SI FINALMANETE NO LA USO AHÍ SERÍA PARAM
    console.log("req.params.id:" , req.params.id);
    console.log("req.params.idcuenta:" , req.params.idcuenta);

    var idcuenta=req.params.idcuenta;
    var idcliente = req.params.id;
    //  console.log ("id: " + id);
    var query = 'q={"$and":[{"idcliente": {"$eq":' + idcliente + '}},{"idcuenta": {"$eq":' + idcuenta +'}}]}'
  //    console.log ("query: " + query);

    var httpClient = requestJson.createClient (mlabBaseURL);
    console.log("Client created");

    console.log (mlabBaseURL + "account?" + query + "&" + mlabAPIKey);
    httpClient.get("account?" + query + "&" + mlabAPIKey,
      function (err, resMlab, body) {
        if (err) {
            var response = {
              "msg": "Error obteniendo cuenta"
            };
            res.status(500);
        } else {
          if (body.length > 0) {
            // var response = body[0];
            var response = body[0];
          } else {
            var response = {
              "msg": "Cuenta no encontrado"
            };
            res.status(404);
          }
        }
        res.send(response);
      }

    )

    }


function createAccountV3 (req, res) {
  console.log("POST /apitechu/v3/products/:id/accounts");

  var idCliente=parseInt(req.params.id);
  // var id = req.body.id;
  console.log("idcliente: " + idCliente);

  var query = 's={"idcuenta":-1}';
  var nexidcuenta=0;

  var httpClient = requestJson.createClient(mlabBaseURL);
  console.log("Client created");

  console.log(mlabBaseURL + "account?" + query + "&" + mlabAPIKey);

  httpClient.get("account?" + query + "&" + mlabAPIKey,
    function (err, resMlab, body) {
      if (err) {
          var response = {
            "msg": "Error obteniendo cuentas"
          };
          res.status(500);
          res.send(response);
      } else {
        if (body.length > 0) {
          console.log("Último id:" ,body[0].idcuenta);
          nexidcuenta = body[0].idcuenta + 1;
        } else {
          nexidcuenta = body[0].idcuenta + 1;
        }

      }

      console.log("Next idcuenta:" , nexidcuenta);

      console.log(req.body.iban);
      console.log(nexidcuenta);
      console.log(idCliente);
      var fechaapertura=dateFormat(new Date(), "dd/mm/yyyy");
      console.log("fechaapertura: ", fechaapertura);
      console.log(req.body.saldo);
      console.log(req.body.divisa);
      console.log(req.body.fechaproxliq);
      console.log(req.body.prodcomercial);
      console.log(req.body.prodfinanciero);
      console.log(req.body.situacion);
      console.log(req.body.fechabaja);


      var newAccount = {
          "iban"         : req.body.iban,
          "idcuenta" : nexidcuenta,
          "idcliente"  : idCliente,
          "fechaapertura"      : fechaapertura,
          "saldo"   : req.body.saldo,
          "divisa"   :req.body.divisa,
          "fechaproxliq"   : req.body.fechaproxliq,
          "prodcomercial"   : req.body.prodcomercial,
          "prodfinanciero"   : req.body.prodfinanciero,
          "situacion"   : req.body.situacion,
          "fechabaja"   : req.body.fechabaja
      };

      var httpClient = requestJson.createClient (mlabBaseURL);
      console.log("Client created");

      httpClient.post("account?" + mlabAPIKey, newAccount,
        function (err, resMlab, body) {
          console.log("Cuenta creada con éxito");
          res.status(201);
          var response = {
            "msg": "Cuenta creada con éxito",
            "idCuenta": nexidcuenta
          }
          res.send(response);
        }

      )
    }
  )
}


    function actualizarsaldoV3(req, res) {
      console.log("actualizarsaldoV3");

      var idcuenta=req.body.idcuenta;
      var idcliente = req.body.idcliente;
      console.log("idcuenta:", idcuenta);
      console.log("idcliente:", idcliente);

      //  console.log ("id: " + id);
      var query = 'q={"$and":[{"idcliente": {"$eq":' + idcliente + '}},{"idcuenta": {"$eq":' + idcuenta +'}}]}'

      var httpClient = requestJson.createClient(mlabBaseURL);
      console.log("Client created");

      console.log(mlabBaseURL + "account?" + query + "&" + mlabAPIKey);

      var cuentaEncontrada = false;

      httpClient.get("account?" + query + "&" + mlabAPIKey,
        function(err, resMlab, body) {
          if (err) {
            var response = {
              "msg": "Error obteniendo cuenta"
            };
            res.status(500);
            res.send(response);
          } else {
            if (body.length > 0) {
              console.log("cuenta encontrada");
              var cuenta = body[0];
              cuentaEncontrada = true;
            } else {
              var response = {
                "msg": "Cuenta no encontrada"
              };
              res.status(404);
              res.send(response);
            }
          }

          if (cuentaEncontrada) {
            console.log("saldo previo cuenta: " + cuenta.saldo);
            console.log("importe del movimiento: " + req.body.importe);

              var flotante = Math.round((cuenta.saldo + req.body.importe) * 100) / 100;
              console.log("flotante:", flotante);
              var importe = flotante.toFixed(2);
              console.log("importe:", importe);
            // parseFloat(cuenta.saldo + req.body.importe);
            // cuenta.saldo = parseFloat(cuenta.saldo + req.body.importe);
            console.log("saldo posterior cuenta : " + importe);
            var putBody = '{"$set":{"saldo":' + importe + '}}';
              // lanzo la petición
              httpClient.put("account?" + query + "&" + mlabAPIKey, JSON.parse(putBody),
                function(errPUT, resMlabPUT, bodyPUT) {
                  console.log("Saldo actualizado con éxito");
                  var response = {
                    "msg": "Saldo actualizado con éxito"
                  }
//comentos este res.send para ver si falla porque solo puede haber un res.send
                  // res.send(response);
                }
              );
          }

        }
      );

    }



  module.exports.getAccountsV3 = getAccountsV3;
  module.exports.getDetailAccountV3 = getDetailAccountV3;
  module.exports.actualizarsaldoV3 = actualizarsaldoV3;
  module.exports.createAccountV3 = createAccountV3;
  module.exports.getAccountsbyibanV3 = getAccountsbyibanV3;
