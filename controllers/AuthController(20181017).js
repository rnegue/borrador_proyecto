const io = require('../io');
const crypt = require('../crypt');

const requestJson = require('request-json');

const mlabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8edrnt/collections/";

const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function loginV1(req, res) {
  console.log("POST /apitechu/v1/login");

  var users = require('../usuarios.json');
  for (user of users) {
    if (user.email == req.body.email &&
      user.password == req.body.password) {
      console.log("Email found, password ok");
      var loggedUserId = user.id;
      user.logged = true;
      console.log("Logged in user with id " + user.id);
      io.writeUserDataToFile(users);
      break;
    }
  }

  var msg = loggedUserId ?
    "Login correcto" : "Login incorrecto, email y/o passsword no encontrados";

  var response = {
    "mensaje": msg,
    "idUsuario": loggedUserId
  };

  res.send(response);
}

function loginV2(req, res) {
  console.log("POST /apitechu/v2/login");

  var email = req.body.email;
  console.log("email: " + email);
  var query = 'q={"email":' + '"' + email + '"}';

  var httpClient = requestJson.createClient(mlabBaseURL);
  console.log("Client created");

  console.log(mlabBaseURL + "user?" + query + "&" + mlabAPIKey);

  var usuarioEncontrado = false;
  var clavecorrecta = false;

  httpClient.get("user?" + query + "&" + mlabAPIKey,
    function(err, resMlab, body) {
      if (err) {
        var response = {
          "msg": "Error obteniendo usuario"
        };
        res.status(500);
      } else {
        if (body.length > 0) {
          console.log("usuario encontrado");
          var user = body[0];
          usuarioEncontrado = true;
        } else {
          var response = {
            "msg": "Usuario no encontrado"
          };
          res.status(404);
        }
      }

      if (usuarioEncontrado) {
        var passwordOK = crypt.checkpassword(req.body.password, user.password)

        if (passwordOK) {
          // Parametro para poner variable logged a true
          var putBody = '{"$set":{"logged":true}}';
          // lanzo la petición
          httpClient.put("user?" + query + "&" + mlabAPIKey, JSON.parse(putBody),
            function(errPUT, resMlabPUT, bodyPUT) {
              console.log("Usuario loggeado con éxito");
              var response = {
                "msg": "Usuario loggeado con éxito",
                "idUsuario": body[0].id
              }
              res.send(response);
            });
        }
        else{
          console.log("Clave incorrecta");
          var response = {
            "msg": "Clave incorrecta"
          }
          res.send(response);


        }
      }
    }
  );
}


function logoutV2(req, res) {
  console.log("POST /apitechu/v2/logout");

  var id = req.body.id;
  console.log("id: " + id);
  var query = 'q={"id":' + id + '}';

  var httpClient = requestJson.createClient(mlabBaseURL);
  console.log("Client created");

  console.log(mlabBaseURL + "user?" + query + "&" + mlabAPIKey);

  var usuarioLogged = false;

  httpClient.get("user?" + query + "&" + mlabAPIKey,
    function(err, resMlab, body) {
      if (err) {
        var response = {
          "msg": "Error obteniendo usuario"
        };
        res.status(500);
      } else {
        if (body.length > 0) {
          console.log("usuario encontrado");
          var user = body[0];
          if (user.logged) {
            usuarioLogged = true;
          } else {
            var response = {
              "msg": "Usuario no loggeado"
            }
            console.log("Usuarios no loggeado");
            // 401 - no permitido
            res.status(401);
            res.send(response);
          }
        } else {
          var response = {
            "msg": "Usuario no encontrado"
          };
          res.status(404);
        }
      }
      if (usuarioLogged) {

        // Parametro para poner variable logged a true
        var putBody = '{"$unset":{"logged":""}}';
        // lanzo la petición
        httpClient.put("user?" + query + "&" + mlabAPIKey, JSON.parse(putBody),
          function(errPUT, resMlabPUT, bodyPUT) {
            var response = {
              "msg": "Usuario desloggeado con éxito",
              "idUsuario": user.id
            }
            console.log("Usuario logout con éxito");
            res.send(response);

          });
      }
    });
}

function logoutV1(req, res) {
  console.log("POST /apitechu/v1/logout");

  var users = require('../usuarios.json');
  for (user of users) {
    if (user.id == req.body.id && user.logged === true) {
      console.log("User found, logging out");
      delete user.logged
      console.log("Logged out user with id " + user.id);
      var loggedoutUserId = user.id;
      io.writeUserDataToFile(users);
      break;
    }
  }

  var msg = loggedoutUserId ?
    "Logout correcto" : "Logout incorrecto";

  var response = {
    "mensaje": msg,
    "idUsuario": loggedoutUserId
  };

  res.send(response);
}

module.exports.loginV1 = loginV1;
module.exports.loginV2 = loginV2;
module.exports.logoutV1 = logoutV1;
module.exports.logoutV2 = logoutV2;
