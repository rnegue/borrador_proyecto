const bcrypt = require('bcrypt');

function hash(data){
  console.log("Hashing data");

  return bcrypt.hashSync(data,10);
}

function checkpassword(sentPassword, userHashPassword) {
  console.log("Checking password");

  return bcrypt.compareSync(sentPassword,userHashPassword);

}

module.exports.hash=hash;
module.exports.checkpassword=checkpassword;
