# Dockerfile apitechu

# Imagen raiz
FROM node

# Carpeta trabajo
WORKDIR /apitechu

# Añado archivos de mi aplicación a imagen
ADD . /apitechu

#Instalo los paquetes necesarios
RUN npm install

# Abrir puerto de la API para que se pueda acceder desde el exterior
EXPOSE 3000

# Comando de inicialización.
CMD ["npm", "start"]
