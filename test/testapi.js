const mocha = require ('mocha');
const chai = require('chai');
const chaihttp =  require ('chai-http');

chai.use(chaihttp);

var should = chai.should();
var server = require ("../server")

/*describe ('First test',
  function () {
    it('Test that Google works', function(done) {
      chai.request ('http://www.google.es')
        .get('/')
        .end(
          function(err, res) {
            console.log("Request finished");
            console.log(err);
          //  console.log(res);
            res.should.have.status(200);
            done();
          }

        )
    }
  )
  }

)
*/

describe ('Test de API de Usuarios',
  function () {
    it('Prueba que la API de usuarios responde', function(done) {
      chai.request ('http://localhost:3000')
        .get('/apitechu/v1/hello')
        .end(
          function(err, res) {
            console.log("Request finished");
            res.should.have.status(200);
            res.body.msg.should.be.eql("Hola desde API TechU!")
            done();
          }

        )
    }
  ),
//   it('Prueba que la API devuelve una lista de usuarios correcta', function(done) {
//     chai.request ('http://localhost:3000')
//       .get('/apitechu/v1/users')
//       .end(
//         function(err, res) {
//           console.log("Request finished");
//           res.should.have.status(200);
//            for (user of res.body.users) {
//              user.should.have.property("email");
//              user.should.have.property("password");
//            }
//
//           done();
//         }
//
//       )
//   }
// )
    it('Prueba API. getDetailUserV3. Recupera detalle de un usuario. Utilizamos un usuario interno (idcliente 1) de pruebas que existe.', function(done) {
      chai.request ('http://localhost:3000')
        .get('/apitechu/v3/users/1')
        .end(
          function(err, res) {
            console.log("Request finished");

            res.should.have.status(200);
            res.body.nombre.should.be.eql("UsuarioPruebas");
            res.body.should.have.property("idcliente");
            res.body.should.have.property("nombre");
            res.body.should.have.property("primer_apellido");
            res.body.should.have.property("segundo_apellido");
            res.body.should.have.property("email");
            res.body.should.have.property("telefono");
            res.body.should.have.property("genero");
            res.body.should.have.property("direccion");
            res.body.should.have.property("poblacion");
            res.body.should.have.property("provincia");
            res.body.should.have.property("codigopostal");
            res.body.should.have.property("foto");
            res.body.should.have.property("codigopostal");

            done();
          }

        )
    }),

    it('Prueba API. getDetailUserV3. Cuando usuario (888888888) no existe.', function(done) {
      chai.request ('http://localhost:3000')
        .get('/apitechu/v3/users/888888888')
        .end(
          function(err, res) {
            console.log("Request finished");

            res.should.have.status(404);
            res.body.msg.should.be.eql("Usuario no encontrado");

            done();
          }

        )
    }),

    it('Prueba API. createUserV3. Crea nuevo usuario.', function(done) {
      chai.request ('http://localhost:3000')
        .post('/apitechu/v3/users')
        .send({
            "dni":"99999998K",
            "email":"prueba@gmail.com",
            "password": "9998",
            "nombre": "nombrePrueba",
            "primer_apellido": "primer_apellidoPrueba",
            "segundo_apellido": "segundo_apellidoPrueba",
            "genero": "generoPrueba",
            "telefono": "999999999",
            "direccion": "direccionPrueba",
            "poblacion": "poblacionPrueba",
            "provincia": "provinciaPrueba",
            "codigopostal": "99999",
            "foto": "https://robohash.org/aspernaturconsequuntursed.png?size=50x50&set=set1",
            "fechaalta": "01/11/2018"
        })
        .end(
          function(err, res) {
            console.log("Request finished");

            res.should.have.status(200);
            res.body.msg.should.be.eql("Usuario creado y loggeado con éxito");

            done();
          }

        )
    }),

    it('Prueba API. createUserV3. Intenta crear un usuario que ya existe', function(done) {
      chai.request ('http://localhost:3000')
        .post('/apitechu/v3/users')
        .send({
            "dni":"99999998K",
            "email":"prueba@gmail.com",
            "password": "9998",
            "nombre": "nombrePrueba",
            "primer_apellido": "primer_apellidoPrueba",
            "segundo_apellido": "segundo_apellidoPrueba",
            "genero": "generoPrueba",
            "telefono": "999999999",
            "direccion": "direccionPrueba",
            "poblacion": "poblacionPrueba",
            "provincia": "provinciaPrueba",
            "codigopostal": "99999",
            "foto": "https://robohash.org/aspernaturconsequuntursed.png?size=50x50&set=set1",
            "fechaalta": "01/11/2018"
        })
        .end(
          function(err, res) {
            console.log("Request finished");

            res.should.have.status(400);
            res.body.msg.should.be.eql("El DNI ya está registrado!");

            done();
          }

        )
    }),

    it('Prueba API. loginV3. Intenta loggear un usuario que ya está loggeado', function(done) {
      chai.request ('http://localhost:3000')
        .post('/apitechu/v3/login')
        .send({
            "dni":"99999998K",
            "password": "9998"
        })
        .end(
          function(err, res) {
            console.log("Request finished");

            res.should.have.status(400);
            res.body.msg.should.be.eql("Usuario ya está loggeado!");

            done();
          }

        )
    }),

    it('Prueba API. loginV3. Cuando usuario (888888888K) no existe', function(done) {
      chai.request ('http://localhost:3000')
        .post('/apitechu/v3/login')
        .send({
            "dni":"88888888K",
            "password": "8888"
        })
        .end(
          function(err, res) {
            console.log("Request finished");

            res.should.have.status(404);
            res.body.msg.should.be.eql("Up!, me temo que este usuario no está registrado en la aplicación!");

            done();
          }

        )
    }),


    it('Prueba API. loginV3. Con clave incorrecta. Usando usuario pruebas (idcliente: 1, dni:"99999999K", password correcta: "9999") ', function(done) {
      chai.request ('http://localhost:3000')
        .post('/apitechu/v3/login')
        .send({
            "dni":"99999999K",
            "password": "9995"
        })
        .end(
          function(err, res) {
            console.log("Request finished");

            res.should.have.status(401);
            res.body.msg.should.be.eql("Clave incorrecta");

            done();
          }

        )
    }),

    it('Prueba API. loginV3. Con clave correcta. Usando usuario pruebas (idcliente: 999999999, dni:"99999999K", password correcta: "9999") ', function(done) {
      chai.request ('http://localhost:3000')
        .post('/apitechu/v3/login')
        .send({
            "dni":"99999999K",
            "password": "9999"
        })
        .end(
          function(err, res) {
            console.log("Request finished");

            res.should.have.status(200);
            res.body.msg.should.be.eql("Usuario loggeado con éxito");

            done();
          }

        )
    }),


    it('Prueba API. logoutV3. Cuando usuario (888888888) no existe.', function(done) {
      chai.request ('http://localhost:3000')
        .post('/apitechu/v3/logout/888888888')
        .end(
          function(err, res) {
            console.log("Request finished");

            res.should.have.status(404);
            res.body.msg.should.be.eql("¡Qué extraño!,no existe el usuario que solicita deslogarse!");

            done();
          }

        )
    }),

    it('Prueba API. logoutV3. Cuando usuario interno de pruebas (idcliente 1) estaba  loggeado.', function(done) {
      chai.request ('http://localhost:3000')
        .post('/apitechu/v3/logout/1')
        .end(
          function(err, res) {
            console.log("Request finished");

            res.should.have.status(200);
            res.body.msg.should.be.eql("Usuario desloggeado con éxito");

            done();
          }

        )
    }),

    it('Prueba API. logoutV3. Cuando usuario ya está deslogeado', function(done) {
      chai.request ('http://localhost:3000')
        .post('/apitechu/v3/logout/1')
        .end(
          function(err, res) {
            console.log("Request finished");

            res.should.have.status(401);
            res.body.msg.should.be.eql("Usuario no loggeado");

            done();
          }

        )
    })


  }
)


/*
const mocha = require ('mocha');
const chai = require('chai');
const chaihttp =  require ('chai-http');

chai.use(chaihttp);

var should = chai.should();
var server = require ("../server")

// describe ('First test',
//   function () {
//     it('Test that Google works', function(done) {
//       chai.request ('http://www.google.es')
//         .get('/')
//         .end(
//           function(err, res) {
//             console.log("Request finished");
//             console.log(err);
//           //  console.log(res);
//             res.should.have.status(200);
//             done();
//           }
//
//         )
//     }
//   )
//   }
//
// )

describe ('Test de API de Usuarios',
  function () {
    it('Prueba que la API de usuarios responde', function(done) {
      chai.request ('http://localhost:3000')
        .get('/apitechu/v1/hello')
        .end(
          function(err, res) {
            console.log("Request finished");
            res.should.have.status(200);
            res.body.msg.should.be.eql("Hola desde API TechU!")
            done();
          }

        )
    }
  )
//   it('Prueba que la API devuelve una lista de usuarios correcta', function(done) {
//     chai.request ('http://localhost:3000')
//       .get('/apitechu/v1/users')
//       .end(
//         function(err, res) {
//           console.log("Request finished");
//           res.should.have.status(200);
//            for (user of res.body.users) {
//              user.should.have.property("email");
//              user.should.have.property("password");
//            }
//
//           done();
//         }
//
//       )
//   }
// )

      // it('Prueba API recupera detalle de un usuario.', function(done) {
      //   chai.request ('http://localhost:3000')
      //     .get('/apitechu/v3/users/14')
      //     .end(
      //       function(err, res) {
      //         console.log("Request finished");
      //         res.should.have.status(200);
      //         console.log("res.body.should.have.property(nombre)");
      //         console.log(res.body.should.have.property("nombre"));
      //         console.log("res.body.nombre:", res.body.nombre);
      //         // console.log("res.body.nombre.should.be.eql("Izaro"):", res.body.nombre.should.be.eql("Izaro"));
      //         //  for (user of res.body.users) {
      //         //    user.should.have.property("idcliente");
      //         //    user.should.have.property("nombre");
      //         //    user.should.have.property("primer_apellido");
      //         //    user.should.have.property("segundo_apellido");
      //         //    user.should.have.property("email");
      //         //    user.should.have.property("telefono");
      //         //    user.should.have.property("genero");
      //         //    user.should.have.property("direccion");
      //         //    user.should.have.property("poblacion");
      //         //    user.should.have.property("provincia");
      //         //    user.should.have.property("codigopostal");
      //         //    user.should.have.property("foto");
      //         //    user.should.have.property("codigopostal");
      //         //  }
      //
      //         done();
      //       }
      //
      //     )
      // })
  }
)
*/
