//console.log ("Hola Mundo");
require('dotenv').config();

const express = require ('express')
const app = express();

app.use (express.json());

var enableCORS = function(req, res, next) {
 // No producción!!!11!!!11one!!1!
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");

 res.set("Access-Control-Allow-Headers","Content-Type");

 next();
}
app.use(enableCORS);

const io = require('./io');
const userController = require ('./controllers/UserController');
const authController = require ('./controllers/AuthController');
const accountController = require ('./controllers/AccountController');
const movementController = require ('./controllers/MovementController');
const transferController = require ('./controllers/TransferController');
const divisaController = require ('./controllers/DivisaController');

const port = process.env.port || 3000;
app.listen(port);

console.log("API escuchando en el puerto BIP BIP " + port);

app.get ("/apitechu/v1/hello",
   function (req, res) {
      console.log("GET /apitechu/v1/hello");

      res.send({"msg": "Hola desde API TechU!"});
    }
  )

  app.get("/apitechu/v3/users/:id", userController.getDetailUserV3);
  app.get("/apitechu/v3/exchange/:sourceCurrency/:tarjetCurrency/:quantity", divisaController.getexchangesV3);

  app.get("/apitechu/v3/acceso/:id", authController.getDetailAccesoV3);


  app.get("/apitechu/v3/products/:id/accounts", accountController.getAccountsV3);

  app.get("/apitechu/v3/accounts/:iban", accountController.getAccountsbyibanV3);
  app.get("/apitechu/v3/products/:id/accounts/:idcuenta", accountController.getDetailAccountV3);

  app.get("/apitechu/v3/products/:id/accounts/:idcuenta/movement", movementController.getMovementsV3);

  app.get("/apitechu/v3/products/:id/accounts/:idcuenta/searchmovements/:fecdesde/:fechasta/:impdesde/:imphasta", movementController.getSearchMovementsV3);

  app.get("/apitechu/v3/products/:id/accounts/:idcuenta/movement/:idmov", movementController.getDetailMovementsV3);

  app.get("/apitechu/v3/products/:id/accounts/:idcuenta/transfer", transferController.getTransfersV3);

  // app.post("/apitechu/v1/users",userController.createUserV1);

  // app.post("/apitechu/v2/users",userController.createUserV2);

  app.post("/apitechu/v3/users", userController.createUserV3);

  app.put("/apitechu/v3/users/:id", userController.updateUserV3);

  app.delete("/apitechu/v3/users/:id", userController.deleteUserV3);

  // app.post("/apitechu/v3/movements",movementController.createMovementV3);
  // app.post("/apitechu/v3/ingreso",movementController.ingresoV3);

  app.post("/apitechu/v3/products/:id/accounts", accountController.createAccountV3);

  app.post("/apitechu/v3/products/:id/accounts/:idcuenta/movement", movementController.createMovementV3);

  app.post("/apitechu/v3/products/:id/accounts/:idcuenta/transfer", transferController.createTransferV3);
  // app.delete ("/apitechu/v1/users/:id",userController.deleteUserV1);

  // app.post("/apitechu/v1/login",authController.loginV1);

  app.post("/apitechu/v3/bajausuario/:id",authController.bajaUsuarioV3);
  app.post("/apitechu/v3/reactivar/:id",authController.reactivarUsuarioV3);
  app.post("/apitechu/v3/login",authController.loginV3);

  // app.post("/apitechu/v1/logout",authController.logoutV1);

// modifico logout para recibir id como parametro y no como body
  app.post("/apitechu/v3/logout/:id",authController.logoutV3);
  app.post("/apitechu/v3/changepassword/:id",authController.changepasswordV3);


/*
  app.post("/apitechu/v1/login",
   function (req, res) {
     console.log("POST /apitechu/v1/login");

     console.log("Body");
     console.log(req.body);

     var users = require('./usuarioslogin.json');
     var encontrado = false
     var password = false

     for (var i = 0; i < users.length; i++) {
        console.log("comparando " + users[i].email + " y " +  req.body.email);
        if (users[i].email == req.body.email) {
          console.log("Encontrado!!");
          encontrado = true
          break;
        }
      }

    var respuesta = {};

    if (encontrado == false)
    {
        console.log("No encontrado");
        respuesta.mensaje = "Login incorrecto. Usuario no existente"
    }
    else if (users[i].password == req.body.password)
        {
           password = true;
           console.log("password OK !!");
           respuesta.mensaje = "Login correcto";
           respuesta.idUsuario = users[i].id;

        }
        else
            {
           password = false;
           console.log("password NOK !!");
           respuesta.mensaje = "Login incorrecto. Password incorrecta"
            }

     if (password)
     {
         users[i].logged = true;
         io.writeUserDataToFile(users);
     }

     res.send (respuesta);

     }
  );

  app.post("/apitechu/v1/logout",
   function (req, res) {
     console.log("POST /apitechu/v1/logout");

     console.log("Body");
     console.log(req.body);

     var users = require('./usuarioslogin.json');
     var logoutOK = false

     for (var i = 0; i < users.length; i++) {
        console.log("comparando " + users[i].id + " y " +  req.body.id);
        console.log("users[i].logged = " + users[i].logged)
        if (users[i].id == req.body.id && users[i].logged) {
          console.log("logout correctoo!!");
          logoutOK = true
          break;
        }
      }

    var respuesta = {};

    if (logoutOK == false)
    {
        console.log("No encontrado");
        respuesta.mensaje = "Logout incorrecto"
    }
    else
        {
           console.log("password OK !!");
           respuesta.mensaje = "Logout correcto";
           respuesta.idUsuario = users[i].id;
           delete users[i].logged;
           io.writeUserDataToFile(users);
        }

    res.send (respuesta);

     }
  );
*/





/*
//forEach ... lo hace entero.

       users.forEach(function(usuario) {


         if (usuario.id == req.params.id)
             {
               var index = users.indexOf(usuario);
               console.log ("usuario= " + JSON.stringify(usuario));
               console.log("Usuario borrado" +  index);
               users.splice(index, 1);
               io.writeUserDataToFile(users);
             }

           });


For ... in itera sobre las propiedades de un objeto peror NO sobre un array de objetos
        var user = users[0];
        console.log(JSON.stringify(user));
         for (const propiedad in user) {
           console.log ("propiedad= " + JSON.stringify(user[propiedad]));

         }

       for (var user of users) {

         if (user.id == req.params.id)
             {
               var index = users.indexOf(user);
               console.log ("usuario= " + JSON.stringify(user));
               console.log("Usuario borrado" +  index);
               users.splice(index, 1);
               io.writeUserDataToFile(users);
             }

       }



        var index = users.findIndex(usuario => usuario.id == req.params.id);
        console.log ("index= " + index);
*/




  app.post("/apitechu/v1/monstruo/:p1/:p2",
     function (req, res) {
        console.log("POST /apitechu/v1/monstruo/:p1/:p2");

        console.log("Parámetros");
        console.log(req.params);

        console.log("Query String");
        console.log(req.query);

        console.log("Headers");
        console.log(req.headers);

        console.log("Body");
        console.log(req.body);

      }
    );




//app.get ("/apitechu/v1/users",

//        req.query ? res.send(users): res.send('no query'); // Operador ternario, if - else
//        res.sendFile('usuarios.json', {root: __dirname});

// estamos esperando una lista de usuarios. V
// por el momento no devolvemos nada  res.send({"msg": "Hola desde API TechU!"});
